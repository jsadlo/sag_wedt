import com.sun.org.apache.xerces.internal.impl.dv.util.HexBin;
import exceptions.InvalidNumberOfFoundElements;
import exceptions.NoMHR;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.*;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class JsoupAmazonBooksAndMusicScrapper {
    private MessageDigest md5;
    static private final String AMAZON_BOOKS_URL =
            "http://www.amazon.com/books-used-books-textbooks/b/ref=sa_menu_bo?ie=UTF8&node=283155";
    static private final String AMAZON_MUSIC_URL =
            "http://www.amazon.com/s/ref=nb_sb_noss?url=search-alias%3Dpopular&field-keywords=";
    static private final String LANGUAGE_LINKS_SELECTOR = "#ref_3291435011";
    static private final String NEXT_PAGE_LINK_SELECTOR = "#pagnNextLink";
    static private final String PRODUCT_LINK_SELECTOR = ".product>.productData>.productTitle>a[href]";
    static private final String PRODUCT_TITLE_SELECTOR = "#btAsinTitle";
    static private final String PRODUCT_MHR_SELECTOR = ".reviews .reviewText";
    private Map<String, String> mhrBooks = new HashMap<String, String>();
    private Map<String, String> mhrMusic = new HashMap<String, String>();

    private Document currentMusicDoc = null;
    private Document currentBooksDoc = null;

    JsoupAmazonBooksAndMusicScrapper() {
        try {
            md5 = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        initBookDoc();
        initMusicDoc();
    }

    public void initBookDoc() {
        initBookDoc(1);
    }

    public void initBookDoc(int scrapFromPage) {
        try {
            Document doc = Jsoup.connect(AMAZON_BOOKS_URL).get();
            Element englishBooksURL = findEnglishBooksUrl(doc);
            currentBooksDoc = Jsoup.connect(englishBooksURL.attr("abs:href")).get();
            currentBooksDoc = goToPage(currentBooksDoc, scrapFromPage);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void initMusicDoc() {
        initMusicDoc(1);
    }

    public void initMusicDoc(int scrapFromPage) {
        try {
            currentMusicDoc = Jsoup.connect(AMAZON_MUSIC_URL).get();
            currentMusicDoc = goToPage(currentMusicDoc, scrapFromPage);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void printYourself() {
        System.out.println("Books:");

        for (String bookTitle : mhrBooks.keySet()) {
            System.out.println("Title: " + bookTitle);
            System.out.println("Review: " + mhrBooks.get(bookTitle));
        }

        System.out.println("Music");
        for (String musicTitle : mhrMusic.keySet()) {
            System.out.println("Title: " + musicTitle);
            System.out.println("Review: " + mhrMusic.get(musicTitle));
        }
    }

    public void saveToFiles(String booksPath, String musicPath) {
        saveToFiles(booksPath, mhrBooks);
        saveToFiles(musicPath, mhrMusic);
    }

    public void saveToFiles(String path, Map<String, String> mhrs) {
        for (String key: mhrs.keySet()) {
            String fileName = "";
            try {
                fileName = new String(HexBin.encode(md5.digest(key.getBytes("UTF-8"))));
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            try {
                File file = new File(path+fileName+".txt");
                BufferedWriter writer = new BufferedWriter(new FileWriter(file));
                writer.write(mhrs.get(key));
                writer.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }

    public void scrapBooks(int scrapNum) {
        mhrBooks.clear();
        try {
            currentBooksDoc = scrap(currentBooksDoc, mhrBooks, scrapNum);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InvalidNumberOfFoundElements e) {
            e.printStackTrace();
        }
    }

    public void scrapMusic(int scrapNum) {
        mhrMusic.clear();
        try {
            currentMusicDoc = scrap(currentMusicDoc, mhrMusic, scrapNum);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InvalidNumberOfFoundElements e) {
            e.printStackTrace();
        }
    }

    private Element findEnglishBooksUrl(Document doc) {
        Element languageLinksList = getSingleElement(doc, LANGUAGE_LINKS_SELECTOR, "language links list");
        Elements languageLinks = languageLinksList.select("a[href]");
        return languageLinks.first();
    }

    private Document goToPage(Document doc, int pageNum) throws IOException {
        int i = 1;
        while (pageNum > i) {
            Element nextPage = getNextPageLink(doc);
            doc = Jsoup.connect(nextPage.attr("abs:href")).get();
            i++;
        }
        return doc;
    }

    private String scrap(String currentURL, Map<String, String> mhrMap, int scrapNum) throws IOException {
        Document currentDoc = Jsoup.connect(currentURL).get();
        while (mhrMap.size() < scrapNum) {
            List<String> productLinks = getProductLinks(currentDoc);

            for (String url : productLinks) {
                Document productDoc = Jsoup.connect(url).get();
                try {
                    mhrMap.put(getTitle(productDoc), getMHR(productDoc));
                } catch (NoMHR e) {}
            }

            Element nextPageLink = getNextPageLink(currentDoc);
            currentDoc = Jsoup.connect(nextPageLink.attr("abs:href")).get();
        }
        return currentDoc.baseUri();
    }

    private Document scrap(Document currentDoc, Map<String, String> mhrMap, int scrapNum) throws IOException {
        while (mhrMap.size() < scrapNum) {
            List<String> productLinks = getProductLinks(currentDoc);

            for (String url : productLinks) {
                Document productDoc = Jsoup.connect(url).get();
                try {
                    mhrMap.put(getTitle(productDoc), getMHR(productDoc));
                } catch (NoMHR e) {}
            }

            Element nextPageLink = getNextPageLink(currentDoc);
            currentDoc = Jsoup.connect(nextPageLink.attr("abs:href")).get();
        }
        return currentDoc;
    }

    private String getTitle (Document productDoc) {
        return getElemText(productDoc, PRODUCT_TITLE_SELECTOR);
    }

    private String getMHR (Document productDoc) {
        Elements allReviews = productDoc.select(PRODUCT_MHR_SELECTOR);
        if (allReviews.size() < 1) {
            throw new NoMHR();
        }
        return allReviews.first().text();
    }

    private String getElemText (Document doc, String selector) {
        return doc.select(selector).text();
    }

    private List<String> getProductLinks(Document doc) {
        Elements productLinks = doc.select(PRODUCT_LINK_SELECTOR);
        List<String> productUrls = new ArrayList<String>(productLinks.size());
        for (Element productLink : productLinks) {
            productUrls.add(productLink.attr("abs:href"));
        }
        return productUrls;
    }

    private Element getNextPageLink (Document doc) {
        return getSingleElement(doc, NEXT_PAGE_LINK_SELECTOR, "next page link");
    }

    private Element getSingleElement (Document doc, String selector, String selectedItemName) {
        Elements selectedList = doc.select(selector);
        if (selectedList.size() != 1) {
            throw new InvalidNumberOfFoundElements(selectedList.size(), selectedItemName);
        }
        return selectedList.first();
    }
}

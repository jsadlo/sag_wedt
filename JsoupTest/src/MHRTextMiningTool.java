import exceptions.ModelNotTrained;
import weka.classifiers.Evaluation;
import weka.classifiers.bayes.NaiveBayesUpdateable;
import weka.core.Instances;
import weka.core.converters.TextDirectoryLoader;
import weka.core.stemmers.SnowballStemmer;
import weka.filters.Filter;
import weka.filters.unsupervised.attribute.StringToWordVector;

import java.io.File;
import java.io.IOException;

public class MHRTextMiningTool {
    private static StringToWordVector filter = new StringToWordVector();

    private NaiveBayesUpdateable model;
    private boolean hasLearned;
    Instances structure;

    MHRTextMiningTool(int wordsToKeep) throws Exception {
        hasLearned = false;
        filter.setWordsToKeep(wordsToKeep);
        filter.setIDFTransform(true);
        filter.setLowerCaseTokens(true);
        filter.setDoNotOperateOnPerClassBasis(true);
        filter.setStemmer(new SnowballStemmer());
        filter.setUseStoplist(true);
    }

    public void learn(String path) throws Exception {
        TextDirectoryLoader loader = loadData(path);

        Instances dataSet = loader.getDataSet();

        if (!hasLearned) {
            filter.setInputFormat(dataSet);
        }

        Instances filtered = Filter.useFilter(dataSet, filter);

        if (!hasLearned) {
            structure = filtered.stringFreeStructure();
            structure.setClassIndex(0);
            model = new NaiveBayesUpdateable();
            model.buildClassifier(structure);
            hasLearned = true;
        }

        for (int i=0; i < filtered.numInstances(); i++) {
            try {
                model.updateClassifier(filtered.instance(i));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void evaluate(String testPath) throws Exception {
        if (!hasLearned) {
            throw new ModelNotTrained();
        }
        TextDirectoryLoader loader = loadData(testPath);
        Instances testData = loader.getDataSet();
        Instances filtered = Filter.useFilter(testData, filter);

        Evaluation eval = new Evaluation(structure);
        eval.evaluateModel(model, filtered);
        System.out.println(eval.toSummaryString("\nResults\n======\n", false));

    }

    private TextDirectoryLoader loadData(String path) throws IOException {
        TextDirectoryLoader loader = new TextDirectoryLoader();
        loader.setDirectory(new File(path));
        return loader;
    }

}

import exceptions.ModelNotTrained;

import java.io.File;
import java.io.IOException;

public class Main {

    private static String baseURL = "/home/tygrysjs/studia/mgr_1/wedt_sag/wedt_sag/data/train/";
    private static String base1URL = "/home/tygrysjs/studia/mgr_1/wedt_sag/wedt_sag/data/train_1/";
    private static String testURL = "/home/tygrysjs/studia/mgr_1/wedt_sag/wedt_sag/data/test/";

    private static String bookDir = "book/";
    private static String musicDir = "music/";


    public static void main(String[] args)
    {
        removeOldData(baseURL);
        removeOldData(base1URL);
        removeOldData(testURL);
        JsoupAmazonBooksAndMusicScrapper scrapper = new JsoupAmazonBooksAndMusicScrapper();
        System.out.println("Train: --------------------------------------");
        scrapMHRsAndSaveToFile(baseURL, 10, scrapper); //save train set
        System.out.println("Train_1: --------------------------------------");
        scrapMHRsAndSaveToFile(base1URL, 30, scrapper); //save train set
        System.out.println("Test: --------------------------------------");
        scrapMHRsAndSaveToFile(testURL, 30, scrapper); //save test set

        try {
            MHRTextMiningTool tool = new MHRTextMiningTool(30);
            tool.learn(baseURL);
            System.out.println("First evaluation:");
            tool.evaluate(testURL);

            tool.learn(base1URL);
            System.out.println("Second evaluation:");
            tool.evaluate(testURL);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ModelNotTrained e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void scrapMHRsAndSaveToFile(String path, int howMany, JsoupAmazonBooksAndMusicScrapper scrapper) {
        scrapper.scrapBooks(howMany);
        scrapper.scrapMusic(howMany);
        scrapper.saveToFiles(path+bookDir, path+musicDir);
    }

    private static void removeOldData(String baseUrl) {
        String bookDirPath = baseUrl + bookDir;
        File bookDir = new File(bookDirPath);
        for (File oldFile : bookDir.listFiles()) {
            oldFile.delete();
        }
        String musicDirPath = baseUrl + musicDir;
        File musicDir = new File(musicDirPath);
        for (File oldFile : musicDir.listFiles()) {
            oldFile.delete();
        }
    }
}

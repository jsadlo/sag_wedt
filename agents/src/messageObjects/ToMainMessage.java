package messageObjects;

import jade.core.AID;
import jade.core.Location;

import java.io.Serializable;

public final class ToMainMessage implements Serializable {
    private final AID agentID;
    private final boolean  isLearning;
    private final Location doneLoc;

    public ToMainMessage(AID agentID, boolean isLearning, Location doneLocation) {
        this.agentID = agentID;
        this.isLearning = isLearning;
        this.doneLoc = doneLocation;
    }

    public AID getAgentID() {
        return agentID;
    }

    public boolean isLearningAgent() {
        return isLearning;
    }

    public boolean isCrawlingAgent() {
        return !isLearning;
    }

    public Location getDoneLoc() {
        return doneLoc;
    }
}

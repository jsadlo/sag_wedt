package messageObjects;

import jade.core.Location;

import java.io.Serializable;

public final class NextLocationLearningMessage implements Serializable {
    private final boolean shouldTestItself;
    private final Location nextLocation;
    private final String dataPath;

    public NextLocationLearningMessage(boolean shouldTestItself, Location nextLocation, String path) {
        this.shouldTestItself = shouldTestItself;
        this.nextLocation = nextLocation;
        this.dataPath = path;
    }

    public boolean shouldTestItself() {
        return shouldTestItself;
    }

    public Location getNextLocation() {
        return nextLocation;
    }

    public String getDataPath() {
        return dataPath;
    }
}

package messageObjects;

import jade.core.Location;
import utils.Configuration;

import java.io.Serializable;

public final class NextLocationCrawlerMessage implements Serializable {
    private static final int defaultHowManyToScrap = Configuration.SCRAP_NUMBER;
    private final Location nextLocation;
    private final String dataPath;
    private final int howManyToScrap;

    public NextLocationCrawlerMessage(Location nextLocation, String path) {
        this.nextLocation = nextLocation;
        this.dataPath = path;
        this.howManyToScrap = defaultHowManyToScrap;
    }

    public NextLocationCrawlerMessage(Location nextLocation, String path, int howManyToScrap) {
        this.nextLocation = nextLocation;
        this.dataPath = path;
        this.howManyToScrap = howManyToScrap;
    }

    public Location getNextLocation() {
        return nextLocation;
    }

    public String getDataPath() {
        return dataPath;
    }

    public int getHowManyToScrap() {
        return howManyToScrap;
    }
}

package agents;

import exceptions.NoContainersToVisit;
import jade.content.ContentElement;
import jade.content.lang.Codec;
import jade.content.lang.sl.SLCodec;
import jade.content.onto.OntologyException;
import jade.content.onto.basic.Action;
import jade.content.onto.basic.Result;
import jade.core.AID;
import jade.core.Location;
import jade.core.ProfileImpl;
import jade.core.Runtime;
import jade.core.behaviours.OneShotBehaviour;
import jade.domain.JADEAgentManagement.QueryPlatformLocationsAction;
import jade.domain.mobility.MobilityOntology;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.lang.acl.UnreadableException;
import jade.wrapper.AgentController;
import jade.wrapper.ControllerException;
import jade.wrapper.PlatformController;
import messageObjects.NextLocationCrawlerMessage;
import messageObjects.NextLocationLearningMessage;
import messageObjects.ToMainMessage;
import utils.Configuration;
import utils.MainAgentDb;
import weka.core.stemmers.SnowballStemmer;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

public class MainAgent extends SagAgent {;
    private static final long serialVersionUID = 1L;

    // there is no point in more crawlers since they would scrap the same data
    private static final int CRAWLER_AGENTS_AMOUNT = 1;
    private static final int LEARNING_AGENTS_AMOUNT = Configuration.LEARNING_AGENT_NUMBER;
    private static final int CONTAINERS_AMOUNT = Configuration.CONTAINER_NUMBER;

    private static long nextCrawlerNumber = 1l;
    private static long nextLearningNumber = 1l;

    private static final boolean autoInitialize = true;
    private boolean initialized = false;

    private MainAgentDb agentDb;

    private List<String> agentNames = new LinkedList<String>();

    public static final int TO_MAIN_MSG_TYPE = ACLMessage.INFORM;
    public static final int FROM_MAIN_MSG_TYPE = ACLMessage.REQUEST;

    @Override
    protected void setup() {
        super.setup();
        agentDb = new MainAgentDb();
        createContainers();
        initWeka();
        getListOfContainers();

        addBehaviour(new MessageHandlingBehavior(TO_MAIN_MSG_TYPE) {
            private static final long serialVersionUID = 1L;

            @Override
            protected void onMessageReceive(ACLMessage msg) {
                addBehaviour(new MainAgentBehaviour(msg));
            }
        });

        addBehaviour(new MessageHandlingBehavior(ACLMessage.REQUEST) {
            @Override
            protected void onMessageReceive(ACLMessage msg) {
                if (!initialized) {
                    addBehaviour(new CreateAgentsBehaviour());
                }
            }
        });

        if (autoInitialize) {
            addBehaviour(new CreateAgentsBehaviour());
        }

    }

    private void createContainers() {
        Runtime rt = Runtime.instance();
        for (int i= 0; i < CONTAINERS_AMOUNT; ++i) {
            rt.createAgentContainer(new ProfileImpl());
        }
        doWait(2000);
    }

    private void getListOfContainers() {
        try {
            getContentManager().registerLanguage(new SLCodec());
            getContentManager().registerOntology(MobilityOntology.getInstance());

            Action action = new Action(getAMS(), new QueryPlatformLocationsAction());

            ACLMessage request = new ACLMessage(ACLMessage.REQUEST);
            request.setLanguage(new SLCodec().getName());
            request.setOntology(MobilityOntology.getInstance().getName());
            getContentManager().fillContent(request, action);
            request.addReceiver(action.getActor());
            send(request);

            ACLMessage receivedMessage = blockingReceive(MessageTemplate.MatchSender(getAMS()));
            ContentElement ce = getContentManager().extractContent(receivedMessage);
            Result result = (Result) ce;
            jade.util.leap.Iterator it = result.getItems().iterator();
            agentDb.addTestLocation((Location)it.next()); // there is at least main container
            while (it.hasNext()) {
                Location loc = (Location)it.next();
                agentDb.addLocation(loc);
            }
        } catch (Codec.CodecException e) {
            e.printStackTrace();
        } catch (OntologyException e) {
            e.printStackTrace();
        }
    }

    private void initWeka() {
        new SnowballStemmer();
    }

    protected void takeDown() {
        super.takeDown();

        // Wiadomosc samozniszczenia do pozostalych agentow
        ACLMessage message = new ACLMessage(selfDestructMessageType);
        for (String agentName : agentNames) {
            message.addReceiver(new AID(agentName, AID.ISGUID));
        }
        send(message);
    }

    // Behaviour odpowiedzialny za obsluge wiadomosci wysylanych do obu rodzajow
    // agentow
    private class MainAgentBehaviour extends OneShotBehaviour {
        private static final long serialVersionUID = 1L;

        private ACLMessage message;

        public MainAgentBehaviour(ACLMessage msg) {
            this.message = msg;
        }

        public void action() {
            if (!initialized) {
                LOGGER.warning("Main agent has not been initialized");
                return;
            }

            ToMainMessage msgContent;
            try {
                msgContent = (ToMainMessage) message.getContentObject();
            } catch (UnreadableException e) {
                LOGGER.warning("MainAgent unable to read message");
                e.printStackTrace();
                return;
            }

            respondToSender(msgContent);
            checkAgentsInQueue(msgContent);
        }

        private void respondToSender(ToMainMessage msgContent) {
            ACLMessage reply = message.createReply();
            reply.setPerformative(FROM_MAIN_MSG_TYPE);
            try {
                if (msgContent.isCrawlingAgent()){
                    NextLocationCrawlerMessage msg = createResponseForCrawler(msgContent);
                    if (msg == null) {
                        return;
                    }
                    reply.setContentObject(msg);
                } else {
                    NextLocationLearningMessage msg = createResponseForLearning(msgContent);
                    if (msg == null) {
                        return;
                    }
                    agentDb.setAgentIsGoingToLocation(msgContent.getAgentID(), msg.getNextLocation());
                    reply.setContentObject(msg);
                }
                send(reply);
            } catch (IOException e) {
                LOGGER.warning("Problem generating response message from MainAgent");
            }
        }

        private void checkAgentsInQueue(ToMainMessage msg) {
            if (shouldLookThroughWaitingAgents(msg)) {
                return;
            }

            Location nextLoc = null;
            AID currentAgent = null;
            boolean shouldTestItself = false;
            int maxIterations = agentDb.getWaitingListSize();

            for (int i = 0; i < maxIterations; ++i) {
                currentAgent = agentDb.getWaitingAgent();
                try {
                    if (agentDb.shouldTestItself(currentAgent)) {
                        nextLoc = agentDb.getTestLocation();
                        shouldTestItself = true;
                    } else {
                        nextLoc = agentDb.getNextLocationForAgent(currentAgent);
                    }
                } catch (NoContainersToVisit e) {
                    agentDb.addWaitingAgent(currentAgent);
                    nextLoc = null;
                    currentAgent = null;
                    shouldTestItself = false;
                    continue;
                }

                if (currentAgent != null && nextLoc != null) {
                    try {
                        ACLMessage sendMsg = new ACLMessage(FROM_MAIN_MSG_TYPE);
                        sendMsg.addReceiver(currentAgent);
                        sendMsg.setContentObject(new NextLocationLearningMessage(shouldTestItself, nextLoc, agentDb.getPathForLocation(nextLoc)));
                        agentDb.setAgentIsGoingToLocation(currentAgent, nextLoc);
                        send(sendMsg);
                    } catch (IOException e) {
                        LOGGER.warning("Problem generating response message from MainAgent");
                    }
                }
            }
        }

        private boolean shouldLookThroughWaitingAgents(ToMainMessage msg) {
            return msg.getDoneLoc() == null || (msg.isLearningAgent() && !agentDb.isAllCrawled());
        }

        private NextLocationCrawlerMessage createResponseForCrawler(ToMainMessage msg) {
            if (msg.getDoneLoc() != null) {
                agentDb.setLocationAsCrawled(msg.getDoneLoc());
            }

            Location loc = agentDb.getNextLocationForCrawling();
            if (loc == null) {  //no more locations to crawl
                return null;
            }

            return new NextLocationCrawlerMessage(loc, agentDb.getPathForLocation(loc));
        }

        private NextLocationLearningMessage createResponseForLearning(ToMainMessage msg) {
            if (msg.getDoneLoc() != null) {
                agentDb.setAgentWasInLocation(msg.getAgentID(), msg.getDoneLoc());
            }

            Location nextLoc = null;
            boolean shouldTest = false;

            try {
                if (agentDb.shouldTestItself(msg.getAgentID())) {
                    nextLoc = agentDb.getTestLocation();
                    shouldTest = true;
                } else {
                    nextLoc = agentDb.getNextLocationForAgent(msg.getAgentID());
                }
            } catch (NoContainersToVisit e) {
                agentDb.addWaitingAgent(msg.getAgentID());
                return null;
            }

            return new NextLocationLearningMessage(shouldTest, nextLoc, agentDb.getPathForLocation(nextLoc));
        }
    }

    private class CreateAgentsBehaviour extends OneShotBehaviour {
        private static final long serialVersionUID = 1L;

        @Override
        public void action() {
            if (!initialized) {
                try {

                    PlatformController container = getContainerController();

                    LOGGER.info("Creating agents: ");
                    for (int i = 1; i <= CRAWLER_AGENTS_AMOUNT; i++) {
                        AgentController ac = createNextCrawlerAgent(container, i);
                        try {
                            sendMessageToCrawler(new AID(ac.getName(), AID.ISGUID));
                        } catch (IOException e) {
                            LOGGER.warning("Problem sending message to crawler.");
                            e.printStackTrace();
                        }
                    }

                    for (int i = 1; i <= LEARNING_AGENTS_AMOUNT; i++) {
                        createNextLearningAgent(container, i);
                    }
                    agentDb.initDataStructures();
                    initialized = true;
                } catch (ControllerException ex) {
                    ex.printStackTrace();
                }
            }
        }

        private void sendMessageToCrawler(AID agent) throws IOException {
            Location loc = agentDb.getNextLocationForCrawling();
            ACLMessage msg = new ACLMessage(FROM_MAIN_MSG_TYPE);
            msg.addReceiver(agent);
            msg.setContentObject(new NextLocationCrawlerMessage(loc, agentDb.getPathForLocation(loc)));
            send(msg);
        }

        private AgentController createNextCrawlerAgent(PlatformController container, int i) throws ControllerException {
            String name = "Crawler" + nextCrawlerNumber;
            LOGGER.info("Agent crawler nr " + i + " has name " + name);
            AgentController ac = container.createNewAgent(name, "agents.CrawlerAgent", null);
            ac.start();
            agentNames.add(ac.getName());
            nextCrawlerNumber++;
            return ac;
        }

        private AgentController createNextLearningAgent(PlatformController container, int i) throws ControllerException {
            String name = "Learner" + nextLearningNumber;
            LOGGER.info("Agent learner nr " + i + " has name " + name);
            AgentController ac = container.createNewAgent(name, "agents.LearningAgent", null);
            ac.start();
            AID agentID = new AID(ac.getName(), AID.ISGUID);
            agentDb.addAgent(agentID);
            agentDb.addWaitingAgent(agentID);
            agentNames.add(ac.getName());
            nextLearningNumber++;
            return ac;
        }
    }
}

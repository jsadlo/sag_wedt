package agents;

import jade.core.AID;
import jade.core.Location;
import jade.core.behaviours.OneShotBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.UnreadableException;
import messageObjects.NextLocationCrawlerMessage;
import messageObjects.ToMainMessage;
import utils.JsoupAmazonBooksAndMusicScrapper;

import java.io.IOException;

public class CrawlerAgent extends SagAgent {
    private static final long serialVersionUID = 1L;

    private static final String bookDir = "book/";
    private static final String musicDir = "music/";

    private String currentDataPath = null;
    private int currentHowManyToScrap = 0;
    private Location currentLocation = null;
    private AID messageSender = null;
    private JsoupAmazonBooksAndMusicScrapper scrapper;

    protected void setup() {
        super.setup();
        scrapper = new JsoupAmazonBooksAndMusicScrapper();


        addBehaviour(new MessageHandlingBehavior(MainAgent.FROM_MAIN_MSG_TYPE) {
            private static final long serialVersionUID = 1L;

            @Override
            protected void onMessageReceive(ACLMessage msg) {
                addBehaviour(new CrawlBehaviour(msg));
            }
        });
    }

    @Override
    protected void takeDown() {
        super.takeDown();
    }

    protected void afterMove() {
        try {
            scrapMHRsAndSaveToFile(currentDataPath, currentHowManyToScrap, scrapper);

            ACLMessage reply = new ACLMessage(MainAgent.TO_MAIN_MSG_TYPE);
            reply.addReceiver(messageSender);
            reply.setContentObject(new ToMainMessage(getAID(), false, currentLocation));
            send(reply);
        } catch (IOException e) {
            LOGGER.warning("Crawler can't send message o MainAgent");
            e.printStackTrace();
        }
    }

    private void scrapMHRsAndSaveToFile(String path, int howMany, JsoupAmazonBooksAndMusicScrapper scrapper) {
        scrapper.scrapBooks(howMany);
        scrapper.scrapMusic(howMany);
        scrapper.saveToFiles(path+bookDir, path+musicDir);
    }

    private class CrawlBehaviour extends OneShotBehaviour {
        private static final long serialVersionUID = 1L;
        private ACLMessage message;

        public CrawlBehaviour(ACLMessage msg) {
            this.message = msg;
        }

        public void action() {
            try {
                NextLocationCrawlerMessage msg = (NextLocationCrawlerMessage) message.getContentObject();
                messageSender = message.getSender();
                currentDataPath = msg.getDataPath();
                currentHowManyToScrap = msg.getHowManyToScrap();
                currentLocation = msg.getNextLocation();
                doMove(msg.getNextLocation());

            } catch (UnreadableException e) {
                LOGGER.warning("Crawler cannot read message");
                e.printStackTrace();
            }
        }
    }
}

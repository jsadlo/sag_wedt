package agents;

import com.google.common.collect.Lists;
import exceptions.ModelNotTrained;
import jade.core.AID;
import jade.core.Location;
import jade.core.behaviours.OneShotBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.UnreadableException;
import messageObjects.NextLocationLearningMessage;
import messageObjects.ToMainMessage;
import utils.Configuration;
import utils.MHRTextMiningTool;

import java.io.IOException;
import java.util.List;

public class LearningAgent extends SagAgent {
    private static final long serialVersionUID = 1L;
    private static final int wordsToKeep = Configuration.ATTRIBUTE_NUMBER;

    private AID messageSender;
    private boolean shouldTest;
    private Location currentLocation;
    private String dataPath;
    private List<String> myVisitPath;

    private MHRTextMiningTool learningTool;

    protected void setup() {
        super.setup();
        try {
            learningTool = new MHRTextMiningTool(30);
        } catch (Exception e) {
            LOGGER.warning(agentName() + "Failed to initialize learning tool");
            e.printStackTrace();
        }
        myVisitPath = Lists.newLinkedList();

        addBehaviour(new MessageHandlingBehavior(MainAgent.FROM_MAIN_MSG_TYPE) {
            private static final long serialVersionUID = 1L;

            @Override
            protected void onMessageReceive(ACLMessage msg) {
                addBehaviour(new LearningAgentBehaviour(msg));
            }

        });

    }

    @Override
    protected void takeDown() {
        super.takeDown();
    }

    private class LearningAgentBehaviour extends OneShotBehaviour {
        private static final long serialVersionUID = 1L;

        private ACLMessage message;

        public LearningAgentBehaviour(ACLMessage msg) {
            this.message = msg;
        }

        public void action() {
            try {
                NextLocationLearningMessage msg = (NextLocationLearningMessage) message.getContentObject();
                messageSender = message.getSender();
                dataPath = msg.getDataPath();
                shouldTest = msg.shouldTestItself();
                currentLocation = msg.getNextLocation();
                doMove(msg.getNextLocation());

            } catch (UnreadableException e) {
                LOGGER.warning("Learner cannot read message");
                e.printStackTrace();
            }
        }
    }

    protected void afterMove() {
        myVisitPath.add(currentLocation.getName());
        try {
            if (shouldTest) {
                evaluate(dataPath);
            } else {
                learn(dataPath);
                ACLMessage reply = new ACLMessage(MainAgent.TO_MAIN_MSG_TYPE);
                reply.addReceiver(messageSender);
                reply.setContentObject(new ToMainMessage(getAID(), true, currentLocation));
                send(reply);
            }
        } catch (IOException e) {
            LOGGER.warning("Learner can't send message o MainAgent");
            e.printStackTrace();
        }
    }

    private void learn(String pathToData) {
        try {
            learningTool.learn(pathToData);
        } catch (IOException e) {
            LOGGER.warning(agentName() + "Failed to load learning data.");
            e.printStackTrace();
        } catch (Exception e) {
            LOGGER.warning(agentName() + "Failed to learn.");
            e.printStackTrace();
        }
    }

    private void evaluate(String pathToData) {
        try {
            String outMsg = "Evaluation from agent " + getLocalName() + ":\n";
            outMsg += "My visit path: " + myVisitPath.toString() + "\n";
            outMsg += learningTool.evaluateAndPlotROC(pathToData, getLocalName());
            LOGGER.info(outMsg);
        } catch (IOException e) {
            LOGGER.warning(agentName() + "Failed to load evaluation data.");
            e.printStackTrace();
        } catch (ModelNotTrained e) {
            LOGGER.warning(agentName() + "Failed to evaluate model - model not trained.");
            e.printStackTrace();
        } catch (Exception e) {
            LOGGER.warning(agentName() + "Failed to evaluate model.");
            e.printStackTrace();
        }
    }
}

package agents;

import jade.core.Agent;
import jade.core.behaviours.CyclicBehaviour;
import jade.core.behaviours.OneShotBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;

import java.util.logging.Logger;

public abstract class SagAgent extends Agent {
    protected static final Logger LOGGER = Logger.getLogger("SagAgent");
    private static final long serialVersionUID = 8956181594409672482L;

    protected static final int selfDestructMessageType = ACLMessage.PROPOSE;

    protected void setup() {
        addBehaviour(new MessageHandlingBehavior(selfDestructMessageType) {
            private static final long serialVersionUID = 1L;

            @Override
            protected void onMessageReceive(ACLMessage msg) {
                SagAgent.this.addBehaviour(new SelfDestructBehavior());
            }
        });
    }

    protected void takeDown() {
        // NOOP
    }

    protected String agentName() {
        return "Agent " + getLocalName() + " ";
    }

    /**
     * Behavior odpowiedzialny za odbior i przetworzenie wiadomosci.
     *
     *
     * Do obslugi wiadomosci mozna podejsc na kilka sposobow.
     *
     * 1. Odbierac dowolna wiadomosc w jednym behaviourze i switchem wybierac
     * odpowiednia obsluge tj. ACLMessage msg = myAgent.receive();
     * switch(msg.getPerformative()) {...}
     *
     * 2. Stworzyc po jednym behaviorze na kazdy typ obslugiwanej wiadomosci i
     * odbierac uzywajac metody z filtrem czyli 
     * MessageTemplate mt = MessageTemplate.MatchPerformative(ACLMessage.CFP);
     * ACLMessage msg = myAgent.receive(mt);
     *
     * W pierwszym przypadku mozna by pokusic sie o blockingReceive(), ale
     * zdecydowalem sie skorzystac z drugiej metody wiec nie rozwijam tego
     * punktu.
     *
     */
    protected abstract class MessageHandlingBehavior extends CyclicBehaviour {
        private static final long serialVersionUID = 1L;

        private int messageType;

        public MessageHandlingBehavior(int messageType) {
            this.messageType = messageType;
        }

        @Override
        public void action() {
            MessageTemplate mt = MessageTemplate.MatchPerformative(messageType);
            ACLMessage msg = myAgent.receive(mt);
            if (msg == null) {
                block();
            } else {
                onMessageReceive(msg);
            }
        }

        /**
         * Reakcja na odebrana wiadomosc
         * @param msg
         * 	wiadomosc
         */
        abstract protected void onMessageReceive(ACLMessage msg);
    }

    private class SelfDestructBehavior extends OneShotBehaviour {
        private static final long serialVersionUID = 1L;

        public void action() {
            LOGGER.info("Agent " + getName() + " konczy dzialanie.");
            doDelete();
        }
    }
}

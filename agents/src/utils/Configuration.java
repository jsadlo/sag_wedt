package utils;

public class Configuration {

    public static final int ATTRIBUTE_NUMBER = 30;
    public static final int SCRAP_NUMBER = 100;
    public static final String BASE_DIR = "/home/tygrysjs/studia/mgr_1/wedt_sag/wedt_sag/data/";
    public static final int CONTAINER_NUMBER = 5;
    public static final int LEARNING_AGENT_NUMBER = 10;
}

package utils;

import exceptions.ModelNotTrained;
import weka.classifiers.Evaluation;
import weka.classifiers.bayes.NaiveBayesUpdateable;
import weka.classifiers.evaluation.ThresholdCurve;
import weka.core.Instances;
import weka.core.Utils;
import weka.core.converters.TextDirectoryLoader;
import weka.core.stemmers.SnowballStemmer;
import weka.filters.Filter;
import weka.filters.unsupervised.attribute.StringToWordVector;
import weka.gui.visualize.PlotData2D;
import weka.gui.visualize.ThresholdVisualizePanel;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.Random;

public class MHRTextMiningTool implements Serializable {
    private StringToWordVector filter = new StringToWordVector();

    private NaiveBayesUpdateable model;
    private boolean hasLearned;
    Instances structure;

    public MHRTextMiningTool(int wordsToKeep) throws Exception {
        hasLearned = false;
        filter.setWordsToKeep(wordsToKeep);
        filter.setIDFTransform(true);
        filter.setLowerCaseTokens(true);
     //   filter.setDoNotOperateOnPerClassBasis(true);
        filter.setStemmer(new SnowballStemmer());
        filter.setUseStoplist(true);
    }

    public void learn(String path) throws Exception {
        TextDirectoryLoader loader = loadData(path);

        Instances dataSet = loader.getDataSet();

        if (!hasLearned) {
            filter.setInputFormat(dataSet);
        }

        Instances filtered = Filter.useFilter(dataSet, filter);

        if (!hasLearned) {
            structure = filtered.stringFreeStructure();
            structure.setClassIndex(0);
            model = new NaiveBayesUpdateable();
            model.buildClassifier(structure);
            hasLearned = true;
        }

        for (int i=0; i < filtered.numInstances(); i++) {
            try {
                model.updateClassifier(filtered.instance(i));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public String evaluateAndPlotROC(String testPath, String agentName) throws Exception {
        if (!hasLearned) {
            throw new ModelNotTrained();
        }
        TextDirectoryLoader loader = loadData(testPath);
        Instances testData = loader.getDataSet();
        Instances filtered = Filter.useFilter(testData, filter);

        Evaluation eval = new Evaluation(structure);
        eval.evaluateModel(model, filtered);

        Evaluation rocEval = new Evaluation(structure);
        rocEval.crossValidateModel(model, filtered, 10, new Random(1));

        // generate curve
        ThresholdCurve tc = new ThresholdCurve();
        int classIndex = 0;
        Instances result = tc.getCurve(rocEval.predictions(), classIndex);

        // plot curve
        ThresholdVisualizePanel vmc = new ThresholdVisualizePanel();
        vmc.setROCString("(Area under ROC = " +
                Utils.doubleToString(tc.getROCArea(result), 4) + ")");
        vmc.setName("ROC from "+agentName);
        PlotData2D tempd = new PlotData2D(result);
        tempd.setPlotName(result.relationName());
        tempd.addInstanceNumberAttribute();
        // specify which points are connected
        boolean[] cp = new boolean[result.numInstances()];
        for (int n = 1; n < cp.length; n++)
            cp[n] = true;
        tempd.setConnectPoints(cp);
        // add plot
        vmc.addPlot(tempd);

        String plotName = vmc.getName();
        final javax.swing.JFrame jf =
                new javax.swing.JFrame("Weka Classifier Visualize: "+plotName);
        jf.setSize(500,400);
        jf.getContentPane().setLayout(new BorderLayout());

        jf.getContentPane().add(vmc, BorderLayout.CENTER);
        jf.addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent e) {
                jf.dispose();
            }
        });

        jf.setVisible(true);

        return eval.toSummaryString("\nResults\n======\n", false);
    }

    private TextDirectoryLoader loadData(String path) throws IOException {
        TextDirectoryLoader loader = new TextDirectoryLoader();
        loader.setDirectory(new File(path));
        return loader;
    }

}

package utils;

import com.google.common.collect.*;
import exceptions.NoContainersToVisit;
import jade.core.AID;
import jade.core.Location;

import java.util.*;

public class MainAgentDb {

    private enum VisitState { NOT_VISITED, IN_PROGRESS, VISITED }

    private static final String basePath = Configuration.BASE_DIR;
    List<AID> agents;
    List<Location> containers;
    Location testLocation;
    boolean wasTestLocationCrawled;
    boolean allCrawled;
    Table<AID, Location, VisitState> historyTable;

    Map<Location, Boolean> wasCrawledThere;
    Map<Location, String> containerDataPaths;

    List<AID> waitingAgents;

    public MainAgentDb() {
        containers = Lists.newArrayList();
        agents = Lists.newArrayList();
        wasCrawledThere = Maps.newHashMap();
        waitingAgents = Lists.newLinkedList();
        containerDataPaths = new HashMap<Location, String>();
        allCrawled = false;
    }

    public void addAgent(AID agent) {
        agents.add(agent);
    }

    public void addLocation(Location loc) {
        containers.add(loc);
        wasCrawledThere.put(loc, Boolean.FALSE);
        containerDataPaths.put(loc, createDefaultPath(loc));
    }

    public void addWaitingAgent(AID agent) {
        waitingAgents.add(agent);
    }

    public AID getWaitingAgent() {
        AID agent = waitingAgents.remove(0);
        return agent;
    }

    public int getWaitingListSize() {
        return waitingAgents.size();
    }

    public void addTestLocation(Location loc) {
        testLocation = loc;
        wasTestLocationCrawled = Boolean.FALSE;
        containerDataPaths.put(loc, createDefaultPath(loc));
    }

    private String createDefaultPath(Location loc) {
        return basePath + loc.getName() + "/";
    }

    public void initDataStructures() {
        historyTable = ArrayTable.create(agents, containers);
        for (AID agent: agents) {
            for (Location loc : containers) {
                historyTable.put(agent, loc, VisitState.NOT_VISITED);
            }
        }
    }

    public void setLocationAsCrawled(Location loc) {
        if (testLocation.equals(loc)) {
            wasTestLocationCrawled = true;
            return;
        }

        wasCrawledThere.remove(loc);
        wasCrawledThere.put(loc, Boolean.TRUE);

        if (wasTestLocationCrawled && !wasCrawledThere.values().contains(Boolean.FALSE)) {
            allCrawled = true;
        }
    }

    public boolean isAllCrawled() {
        return allCrawled;
    }

    public void setAgentIsGoingToLocation(AID agent, Location loc) {
        if (testLocation.equals(loc)) {
            return;
        }

        historyTable.put(agent, loc, VisitState.IN_PROGRESS);
    }

    public void setAgentWasInLocation(AID agent, Location loc) {
        historyTable.put(agent, loc, VisitState.VISITED);
    }

    public String getPathForLocation(Location loc) {
        if (loc == null) {
            return "";
        }
        return containerDataPaths.get(loc);
    }

    public boolean shouldTestItself(AID agent) {
        return numberOfVisitedLocations(agent) > containers.size()/2;
    }

    public Location getTestLocation() throws NoContainersToVisit {
        if (!wasTestLocationCrawled) {
            throw new NoContainersToVisit();
        }
        return testLocation;
    }

    public Location getNextLocationForCrawling() {
        if (!wasTestLocationCrawled && wasCrawledThere.values().contains(Boolean.TRUE)) {
            return testLocation;
        }

        for (Location loc : wasCrawledThere.keySet()) {
            if (!wasCrawledThere.get(loc)) {
                return  loc;
            }
        }
        return null;
    }

    public Location getNextLocationForAgent(AID agent) throws NoContainersToVisit {
        List<Location> orderedLocationsNotVisitedByAgent =  getOrderedLocationsNotVisitedByAgent(agent);
        if (orderedLocationsNotVisitedByAgent.size() == 0) {
            throw new NoContainersToVisit();
        }
        return orderedLocationsNotVisitedByAgent.get(0);
    }

    public List<Location> getOrderedLocationsNotVisitedByAgent(AID agent) {
        Map<Location, VisitState> locationsForAgent = historyTable.row(agent);
        Map<Location, Integer> notVisitedLocationsThatAreNotInProgress = Maps.newHashMap();
        for (Location loc: locationsForAgent.keySet()) {
            if (locationCanBeVisited(locationsForAgent.get(loc), loc)){
                notVisitedLocationsThatAreNotInProgress.put(loc, numberOfVisits(loc));
            }
        }
        final Map<Location, Integer> finalNotVisitedLocations = Maps.newHashMap(notVisitedLocationsThatAreNotInProgress);
        List<Location> notVisitedList = Lists.newArrayList(notVisitedLocationsThatAreNotInProgress.keySet());
        Collections.sort(notVisitedList, new Comparator<Location>() {
            @Override
            public int compare(Location location, Location location2) {
                return finalNotVisitedLocations.get(location).compareTo(finalNotVisitedLocations.get(location2));
            }
        });
        return notVisitedList;
    }

    public boolean locationCanBeVisited(VisitState state, Location loc) {
        boolean isInProgress = historyTable.column(loc).values().contains(VisitState.IN_PROGRESS);
        boolean ifNotAllIsCrawledIsNotVisited = allCrawled ? true : numberOfVisits(loc) == 0;
        return state == VisitState.NOT_VISITED && !isInProgress && wasCrawledThere.get(loc) && ifNotAllIsCrawledIsNotVisited;
    }

    private int numberOfVisits(Location loc) {
        Multiset<VisitState> valuesSet = HashMultiset.create(historyTable.column(loc).values());
        return valuesSet.count(VisitState.VISITED);
    }

    private int numberOfVisitedLocations(AID agent) {
        Multiset<VisitState> valuesSet = HashMultiset.create(historyTable.row(agent).values());
        return valuesSet.count(VisitState.VISITED);
    }
}

package myExamples;

import jade.core.Agent;

/**
 This example shows a minimal agent that just prints "Hallo World!"
 and then terminates.
 @author Giovanni Caire - TILAB
 */
public class HelloWorld extends Agent {

    protected void setup() {
        System.out.println("Hello World! My name is gupi jestem strasznie :P "+getLocalName());

        // Make this agent terminate
        doDelete();
    }
}
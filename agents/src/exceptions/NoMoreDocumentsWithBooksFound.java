package exceptions;

public class NoMoreDocumentsWithBooksFound extends RuntimeException {

    public NoMoreDocumentsWithBooksFound() {
        super();
    }
}

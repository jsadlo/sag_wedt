package exceptions;

public class ModelNotTrained extends RuntimeException {

    public ModelNotTrained() {
        super("This model has not been trained. It cannot be evaluated.");
    }
}

package exceptions;

public class InvalidNumberOfFoundElements extends RuntimeException {
    public InvalidNumberOfFoundElements(Integer foundLinksNumber, String searchName) {
        super("There were " + foundLinksNumber.toString() + " elements when looking for " + searchName + ".");
    }
}
